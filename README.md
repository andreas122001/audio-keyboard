# Audio Keyboard

Translates live audio to a press on the keyboard.

This project contains a simple python-file that will listen for live audio, find the frequency and control the keyboard based on the note being played.

The idea here is that you can play a game on your computer with a guitar or other instrument.

This is just a simple project I did to learn python.

**NOTE**: to run this yourself, you will need to have python installed (I am using Anaconda3 with Python 3.9.12) and you need to install the packages in the imports: e.g.: 
```
pip3 install scipy
pip3 install matplotlib
pip3 install pyaudio
pip3 install numpy
pip3 install keyboard
```
