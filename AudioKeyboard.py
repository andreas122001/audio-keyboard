import matplotlib.pyplot as plt
import pyaudio
import numpy as np
from scipy.fft import fft, fftfreq
from collections import deque
import keyboard as k

def find_note(x):
    notes = ['A', 'A#', 'B', 'C', 'C#', 'D', 'D#', 'E', 'F', 'F#', 'G', 'G#', 'n/a']

    frequencies = [
		[55.00, 110.00, 220.00, 440.00, 880.00],
		[58.27,116.54, 233.08, 466.16, 932.32],
		[61.74, 123.48, 246.96, 493.92, 987.84],
		[65.41, 130.82, 261.64, 523.28, 1046.56],
		[69.30, 138.60, 277.20, 554.40, 1108.80],
		[73.42, 146.84, 293.68, 587.36, 1174.72],
		[77.78, 155.56, 311.12, 622.24, 1244.48],
		[82.41, 164.82, 329.64, 659.28, 1318.56],
		[87.31, 174.62, 349.24, 698.48, 1396.96],
		[92.50, 185.00, 370.00, 740.00, 1480.00],
		[98.00, 196.00, 392.00, 784.00, 1568.00],
		[103.83, 207.66, 415.32, 830.64, 1661.28]]

    if (x < 53.5):
        return notes[-1], x-frequencies[0][0]
    if x > 1700:
        return notes[-1], x-frequencies[-1][-1]
        
    min_distance = 100
    note_index = -1
    note_number = -1
    for i in range(len(frequencies)):
        for j in range(len(frequencies[0])):
            freq = frequencies[i][j]
            distance = abs(x-freq)
            if (distance < min_distance):
                note_index = i
                note_number = j
                min_distance = distance
    note = notes[note_index]
    dist = x-frequencies[note_index][note_number]
    return note, dist

def fft_normal(data, rate, lim = 0):
    size = len(data)
    xf = abs(fftfreq(size, 1/rate))
    yf = 2*abs(fft(data)) / size
    freq = xf[np.where(yf >= lim)]
    freq = np.unique(freq)
    return freq, xf, yf

def find_max(x1, y1):
    f = np.unique(x1[np.where(y1 >= np.max(y1))])[0]
    amp = np.max(y1)
    if amp > 100:
        return f, amp
    else: 
        return 0, amp

def read_wav(size, file):
    data = file.readframes(size)
    data = np.frombuffer(data, 'int16')
    return data

def read(stream, size):
    data = stream.read(size)
    data = np.frombuffer(data, 'int16')
    return data
    

chunk = 2048
rate = 65536

freq_buffer_size = 10

buffer = deque([0]*rate, rate)
notes = deque([0]*freq_buffer_size, freq_buffer_size)

p = pyaudio.PyAudio()

stream = p.open(format=pyaudio.paInt16, channels=1, rate=rate, input=True)

plt.ion()

data = read(stream, rate)
for i in data:
    buffer.append(i)
while stream.is_active:
    data = read(stream, chunk)
    for i in data:
        buffer.append(i)

    freq, x, y = fft_normal(buffer, rate, 20)

    f, a = find_max(x, y)
    note, dist = find_note(f)
    print('\r'+note + ' (' + ('+' if dist >= 0 else '') + str(round(dist)) + 'Hz) ' + str(round(a)) +' dB', end='      ')

    keymap = {'F#': 'w', 'A#': 'a', 'B': 's', 'C': 'd',
                'E': 'p', 'G': 'l', 'G#': 'ø', 'A': 'æ'}

    list1 = list(keymap.values())

    key = keymap.get(note, '')
    if key in list1:
        k.press(key)
        list1.remove(key)
        print('-KEY('+key+')')

    for i in list1:
        k.release(i)
